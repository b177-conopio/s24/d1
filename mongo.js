// Query Operators

// [SECTION] Comparison Query Operators
// $gt and $gte operator (Greater than/Greater than or equal to)

// find users with an age greater than 50
db.users.find({
	age: {
		$gt: 50
	}
})

db.users.find({
	age: {
		$gte: 50
	}
})

// find users with an age less than 50
db.users.find({
	age: {
		$lt: 50
	}
})

db.users.find({
	age: {
		$lte: 50
	}
})

// find users with an age that is NOT 82
db.users.find({
	age: {
		$ne: 82
	}
})

// find users whose last names are either "Hawking" or "Doe"
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})

db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})

// find users whose courses including "HTML" or "React"
db.users.find({
	courses: {
		$in: ["HTML", "React"]
	}
})

[SECTION] Logical Query Operators

// $or operator
db.users.find({
	$or:[
	{
		firstName: "Neil"
	},
	{
		age: 21
	}
	]
})

// $and operator
db.users.find({
	$and: 
	[{age: {$ne: 82}}	,
		{age: {$ne: 76}}
	]
})

// May 23, 2022
// Exclusion
/*
 - Allows us to exclude/remove specific fields only when retrieving documents.
 - The value provided is 0 to denote that the field is being included.
 - Syntax db.users.find({criteria},{field: 0})

*/

db.users.find({
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1
})

db.users.find({
	firstName: "Jane"
},
{
	firstName: 0,
	lastName: 0,
	contact: 0
})

db.users.find({
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
})

// Supressing the ID field
/*	- allows to exclude "_id" field when retrieving documents.
	- When using the field projection, field inclusion and exclusion may not be used at the same time

	- Excluding the _id field is the only exception to this rule
*/

db.users.find({
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1
	_id: 0
})

// Returning specific fields in embedded documents
db.users.find({
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
})

// Suppressing specific fields in embedded documents
db.users.find({
	firstName: "Jane"
},
{
	"contact.phone": 0
})

// Project Specific Array Elements in the Returned Array
// The $slice operator allows us to retrieve
// Only 1 element that matches the search criteria

db.users.find(
	{"namearr":
		{
			namea: "juan"
		}
	},
	{"namearr":
		{$slice: 1}
		}

	)

// $regex operator

/*
	- Allows us to find documents that much a specific string pattern using regular expression

	Syntax:
		db.users.find({field: $regex 'pattern', $options: '$optionValue'});

*/

// Case sensitive query
db.users.find({firstName: {$regex: 'N'}}).pretty();

// Case insensitive query
db.users.find({firstName: {$regex: 'j', $options: '$i'}});

db.users.find({
	$or: [
		{firstName: {$regex: 's', $options: '$i'}},
		{lastName: {$regex: 'd', $options: '$i'}}
	]
}, {firstName: 1, lastName: 1, _id: 0}).pretty();

// S24 activity
// Find users who are from HR department and their age is greater than or equal to 70

// Find users with letter 'e' in their first name and has an age less than or equal to 30

// Slice on array
db.users.find(
{ firstName: "Jane"},
{ courses: { $slice: 1}}
)